# 2023_dev_android_produits


Petite application Android permettant d'introduire l'utilisation de l'ORM Room pour la gestion d'une base de données.
L'application permet de gérer un ensemble de produits.

Contexte : Cours de développement Mobile  - R4.A.11 - Fiche 5 - 2023 - J.-F. Condotta