package iutlens.android.produits

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import kotlin.random.Random

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var bt_initialiser: Button
    private lateinit var bt_lister_produits: Button
    private lateinit var bt_lister_quantite: Button
    private lateinit var bt_inserer_produit: Button
    private lateinit var bt_quitter: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        bt_initialiser=findViewById(R.id.id_bt_initialiser_bd)
        bt_lister_produits=findViewById(R.id.id_bt_lister_produits)
        bt_inserer_produit=findViewById(R.id.id_bt_inserer_produit)
        bt_quitter=findViewById(R.id.id_bt_quitter)
        bt_lister_quantite=findViewById(R.id.id_bt_lister_quantite)
        bt_initialiser.setOnClickListener(this)
        bt_lister_produits.setOnClickListener(this)
        bt_inserer_produit.setOnClickListener(this)
        bt_lister_quantite.setOnClickListener(this)
        bt_quitter.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        if (v.id==R.id.id_bt_initialiser_bd) {
            val db = ProduitsDB.getDatabase(applicationContext)
            val produitDAO = db.produitDao()
            produitDAO.delete()
            for (i in 1..50)
                produitDAO.insert(Produit(i,"Produit "+i, Random.nextInt(50),Random.nextInt(1000)+1.0))
        } else if  (v.id==R.id.id_bt_lister_produits) {
            val intent = Intent(applicationContext, ListerProduitsActivity::class.java)
            startActivity(intent)
        }  else if  (v.id==R.id.id_bt_lister_quantite) {
            val intent = Intent(applicationContext, ListerProduitsQuantiteActivity::class.java)
            startActivity(intent)
        } else if  (v.id==R.id.id_bt_inserer_produit) {
        val intent = Intent(applicationContext, InsererProduitActivity::class.java)
        startActivity(intent)
    }else if  (v.id==R.id.id_bt_quitter) {
            finish()
        }
    }
}