package iutlens.android.produits

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

@Database(entities = [Produit::class], version = 1)
@TypeConverters(ConvertisseursProduits::class)
abstract class ProduitsDB : RoomDatabase() {
    abstract fun produitDao(): ProduitDAO
    companion object {
        private var INSTANCE_DB: ProduitsDB? = null
        fun getDatabase(context: Context): ProduitsDB {
            if (INSTANCE_DB == null) {
                synchronized(this) {
                    INSTANCE_DB =
                        Room.databaseBuilder(context,ProduitsDB::class.java, "produits_database")
                            .allowMainThreadQueries()
                            .addTypeConverter(ConvertisseursProduits())
                            .build()
                }
            }
            return INSTANCE_DB!!
        }
    }
}
