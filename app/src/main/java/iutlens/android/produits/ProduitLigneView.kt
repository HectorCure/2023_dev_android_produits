package iutlens.android.produits

import android.content.Context
import android.graphics.Color
import android.view.Gravity
import android.view.View
import android.widget.TableRow
import android.widget.TextView
import java.text.SimpleDateFormat

class ProduitLigneView {
    companion object{
        fun celluleEnTete(texte:String,context:Context): TextView {
            val cellule= TextView(context)
            cellule.setText(texte)
            cellule.setTextColor(Color.DKGRAY)
            cellule.setBackgroundColor(Color.YELLOW)
            cellule.setPadding(7, 5, 5, 5)
            cellule.setTextSize(20.0F)
            cellule.textAlignment= View.TEXT_ALIGNMENT_CENTER
            return cellule
        }
        fun ligneEnTete(context:Context, vararg textes:String): TableRow {
            val ligne= TableRow(context)
            textes.forEach {ligne.addView(celluleEnTete(it,context)) }
            ligne.setHorizontalGravity(Gravity.CENTER_HORIZONTAL)
            return ligne
        }
        fun celluleNormale(texte:String,context:Context): TextView {
            val cellule= TextView(context)
            cellule.setText(texte)
            cellule.setTextColor(Color.BLUE)
            cellule.setBackgroundColor(Color.WHITE)
            cellule.setPadding(7, 5, 5, 5)
            cellule.setTextSize(20.0F)
            cellule.textAlignment= View.TEXT_ALIGNMENT_CENTER
            return cellule
        }
        fun ligneProduit(produit:Produit,context: Context): TableRow {
            val ligne= TableRow(context)
            ligne.addView(celluleNormale(produit.id.toString(),context))
            ligne.addView(celluleNormale(produit.nom.toString(),context))
            ligne.addView(celluleNormale(produit.quantite.toString(),context))
            ligne.addView(celluleNormale(produit.prix.toString(),context))
            val formateurDate = SimpleDateFormat("dd-MM-YY")
            ligne.addView(celluleNormale(formateurDate.format(produit.date),context))
            ligne.setHorizontalGravity(Gravity.CENTER_HORIZONTAL)
            return ligne
        }
    }
}