package iutlens.android.produits

import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import iutlens.android.produits.ProduitLigneView.Companion.ligneEnTete
import iutlens.android.produits.ProduitLigneView.Companion.ligneProduit


class ListerProduitsActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var bt_quitter: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lister_produits)
        bt_quitter=findViewById(R.id.id_bt_quitter_lister_produits)
        bt_quitter.setOnClickListener(this)
        remplirTable()
    }
    override fun onClick(v: View) {
        if (v.id==R.id.id_bt_quitter_lister_produits) {
            finish()
        }
    }

    fun remplirTable() {
        val table: LinearLayout =findViewById(R.id.id_table_lister_produits)
        table.addView(ligneEnTete(this,"id","nom","quantite","prix","date création"))
        val db = ProduitsDB.getDatabase(applicationContext)
        val produits=db.produitDao().listeProduits()
        for (produit in produits)
            table.addView(ProduitLigneView.ligneProduit(produit, this))
    }
}