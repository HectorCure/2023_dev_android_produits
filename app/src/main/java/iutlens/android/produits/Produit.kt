package iutlens.android.produits

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*


@Entity(tableName = "produit")
data class Produit(
    @PrimaryKey
    @ColumnInfo(name = "id_prod")
    val id : Int,
    @ColumnInfo(name = "nom_prod")
    val nom : String,
    @ColumnInfo(name = "quantite_prod")
    val quantite : Int,
    @ColumnInfo(name = "prix_prod")
    val prix : Double,
    @ColumnInfo(name = "date_creation_prod")
    val date : Date = Date()
)
