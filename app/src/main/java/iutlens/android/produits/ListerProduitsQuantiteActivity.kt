package iutlens.android.produits

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.LinearLayout

class ListerProduitsQuantiteActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var bt_quitter: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lister_produits_quantite)
        bt_quitter=findViewById(R.id.id_bt_quitter_lister_produits_quantite)
        bt_quitter.setOnClickListener(this)
        remplirTable()
    }
    override fun onClick(v: View) {
        if (v.id==R.id.id_bt_quitter_lister_produits_quantite)
            finish()
    }

    fun remplirTable() {
        val table: LinearLayout =findViewById(R.id.id_table_lister_produits_quantite)
        table.addView(ProduitLigneView.ligneEnTete(this, "id", "nom", "quantite", "prix","date création"))
        val db = ProduitsDB.getDatabase(applicationContext)
        val produits=db.produitDao().listeProduitsAvecQuantite(20)
        for (produit in produits)
            table.addView(ProduitLigneView.ligneProduit(produit, this))
    }
}