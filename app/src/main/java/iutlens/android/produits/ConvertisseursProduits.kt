package iutlens.android.produits

import androidx.room.ProvidedTypeConverter
import androidx.room.TypeConverter
import java.util.*


@ProvidedTypeConverter
class ConvertisseursProduits {
        @TypeConverter
        fun longToDate(value: Long): Date {
            return Date(value) }

        @TypeConverter
        fun dateToLong(date: Date): Long {
            return date.time
        }
}
