package iutlens.android.produits

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query

@Dao
interface ProduitDAO {
    @Insert
    fun insertAll(vararg produits: Produit)

    @Insert
    fun insert(produit: Produit)

    @Delete
    fun delete(produit: Produit)

    @Query("DELETE FROM produit")
    fun delete()

    @Query("SELECT MAX(id_prod) FROM produit")
    fun maxId():Int

    @Query("SELECT * FROM produit ORDER BY id_prod")
    fun listeProduits(): List<Produit>

    @Query("SELECT * FROM produit WHERE quantite_prod>:quantite ORDER BY id_prod")
    fun listeProduitsAvecQuantite(quantite:Int): List<Produit>
}