package iutlens.android.produits

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import kotlin.random.Random

class InsererProduitActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var et_nom : EditText
    private lateinit var et_prix : EditText
    private lateinit var et_quantite : EditText
    private lateinit var bt_inserer_inserer: Button
    private lateinit var bt_inserer_annuler: Button
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_inserer_produit)
        et_nom=findViewById(R.id.id_et_inserer_nom)
        et_prix=findViewById(R.id.id_et_inserer_prix)
        et_quantite=findViewById(R.id.id_et_inserer_quantite)
        bt_inserer_annuler=findViewById(R.id.id_bt_inserer_annuler)
        bt_inserer_inserer=findViewById(R.id.id_bt_inserer_inserer)
        bt_inserer_annuler.setOnClickListener(this)
        bt_inserer_inserer.setOnClickListener(this)
    }
    override fun onClick(v: View) {
        if (v.id==R.id.id_bt_inserer_inserer) {
            val textNom=et_nom.text
            val  textPrix=et_prix.text
            val textQuantite=et_quantite.text
            if (textNom.isNotEmpty() && textPrix.isNotEmpty() && textQuantite.isNotEmpty()) {
                val db = ProduitsDB.getDatabase(applicationContext)
                val produitDAO = db.produitDao()
                produitDAO.insert(Produit(produitDAO.maxId()+1,textNom.toString(),textQuantite.toString().toInt(),textPrix.toString().toDouble()))
                finish()
            }
        } else if (v.id==R.id.id_bt_inserer_annuler) {
            finish()
        }
    }
}